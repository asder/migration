import {r} from 'rethinkdb-ts';
const json = require('./app_extensions.json');

async function main() {

    const connection = await r.connect({
        host: 'localhost',
        password: 'rethinkdb',
        db: 'app_extensions',
        port: 28015
    });



   await r.table('app_extensions').insert(json.RECORDS).run(connection);
   const res = await r.table('app_extensions').run(connection);
   console.log(res);
}

main();